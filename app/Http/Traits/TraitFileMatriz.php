<?php

namespace App\Http\Traits;
use App\Http\Controllers\Test\Cell;

trait TraitFileMatriz {

    public function getDataFile($request){
        
        $file = $request->file('file');

        $file =  fopen($file->getPathName(), "r");

        $data = [];
        
        while (!feof($file)){
            $line = fgets($file);
            $line = str_replace("\r\n", "", $line);
            array_push($data, explode(" ",$line));
        }
        
        fclose($file);
        
        return $data;
    }

    public function getMatriz($matriz){
        foreach($matriz AS $rowI => $rowV){
            foreach($rowV AS $colI => $colV){
                $obj = new Cell($rowI, $colI, $rowV);
                $matriz[$obj->X][$obj->Y] = $obj;
            }
        }

        return $matriz;
    }

    public function hallsWalk($matriz){    
        //empezamos recorriendo la matriz
        foreach($matriz AS $rowI => $rowV){
            for($i = 0; $i < count($rowV); $i++){
                $obj = $matriz[$rowI][$i];
                if($obj->isHall()){
                    if($obj->isIlluminate()){
                        continue;
                    }
                    
                    $obj->setIlluminate(true);
                    $obj->setFocus(true);

                    $right = $obj->getRight($matriz);

                    while($right && $right->hall){
                        $right->setIlluminate(true);
                        
                        $right = $right->getRight($matriz);
                    }
                    
                    $bottom = $obj->getBottom($matriz);
                    
                    while($bottom && $bottom->hall){
                        $bottom->setIlluminate(true);
                        
                        $bottom = $bottom->getBottom($matriz);
                    }                    
                }
            }
        }

        return $matriz;
    }
}