<?php

namespace App\Http\Controllers\Test;

class Cell {
    public $X;
    public $Y;
    public $hall = false;
    public $wall = false;
    public $focus = false;
    public $illuminate = false;
    public $bottom = null;
    public $top = null;
    public $left = null;
    public $right = null;

    public function __construct($x = null, $y = null, $cell = null){
        // dd($this);
        $this->X = $x;
        $this->Y = $y;
        $this->hall = $cell[$y] != 1;
        $this->wall = $cell[$y] == 1;
        $this->focus = false;
        $this->illuminate = false;

        return $this;
    }

    public function isIlluminate(){
        return $this->illuminate;
    }

    public function setIlluminate($value){
        return $this->illuminate = $value;
    }

    public function setFocus($value){
        return $this->focus = $value;
    }

    public function isHall(){
        return $this->hall;
    }

    public function isWall(){
        return $this->wall;
    }

    public function getRight($matriz){
        return (!isset($matriz[$this->X][$this->Y+1])) ? false : $matriz[$this->X][$this->Y+1];
    }

    public function getBottom($matriz){
        return (!isset($matriz[$this->X+1][$this->Y])) ? false : $matriz[$this->X+1][$this->Y];
    }
}