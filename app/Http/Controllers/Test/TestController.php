<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Http\Traits\TraitFileMatriz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use App\Exceptions\ValidationError;

class TestController extends Controller
{
    use TraitFileMatriz;

    public $rules = [
        'file' => 'required',
    ];

    public $labels = [
        'file' => 'Archivo',
    ];

    public function index(){        
        return inertia('Test/Index');
    }

    public function uploadFile(Request $request){
        try {
            $validator = Validator::make(['file' => $request->file('file')], $this->rules, [], $this->labels);
            
            if($validator->fails()){
                throw new ValidationError($validator->errors());
            }

            //obtenemos el data del file con la matriz
            $data = $this->getDataFile($request);

            //procesamos la matriz con sus propiedades
            $matriz = $this->getMatriz($data);

            //recorrido de pasillos
            $matriz = $this->hallsWalk($matriz);
        } catch (ValidationError $e) {
            return Response::json([ "success" => false, 
                                    "msg" => "ERROR: PROPORCIONA LOS CAMPOS CORRECTOS", 
                                    "errors" => $e->getErrors()
                                  ]);
        }catch (\Exception $e){
            return Response::json([ "success" => false, 
                                    "msg" => $e->getMessage()
                                  ]);

        }
            return Response::json(['success'=>true, 'msg' => '', 'matriz' => $matriz]);
    }
}
