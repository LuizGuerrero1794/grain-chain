## Requerimientos
    Composer v1.8.6
    Apache v2.4.35
    PHP v7.4.3
    MySQL v5.7.24
    NodeJs v12.14.0
    NPM v6.13.4


## Instalacion

Clonar el repositorio:

```sh
git clone https://gitlab.com/LuizGuerrero1794/grain-chain.git grain-chain
cd grain-chain
```

Instalar dependencias de PHP:

```sh
composer install
```

Instalar dependencias de NPM:

```sh
npm install
```

Compilar Proyecto:

```sh
npm run dev
```

Configurar variables de entorno:

```sh
cp .env.example .env
```

Generar key de aplicacion:

```sh
php artisan key:generate
```

Crear base de datos (MySQL) con algun gestor y agregar los datos de conexion en el archivo de configuracion .env:

```sh
CREATE DATABASE grain_chain

//.env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=grain_chain
DB_USERNAME=root
DB_PASSWORD=
```

Ejecutar migraciones:

```sh
php artisan migrate:fresh --seed
```

**validar que se encuentre activada la extension 'exif' de PHP**


Correr el servidor de desarrollo y con la direccion http de salida, acceder desde el navegador:
git 
```sh
php artisan serve
```

## Usuario

email: grain.chain@example.com

password: secret